var minDate = new Date(2014, 0, 1, 0, 0, 0, 0);
var maxDate = new Date(2015, 0, 1, 0, 0, 0, 0);
var delta = maxDate.getTime() - minDate.getTime();

var job_id = arg2; //nombre del job

var documentNumber = arg1; //cantidad de documentos a enviar

var documentName = name; //nombre del documento en la base

var batchNumber = batch; //cantidad a insertar al mismo tiempo (RAM)


var job_name = 'Job#' + job_id;
var start = new Date();

var batchDocuments = new Array();
var index = 0;

print('Comenzando job:' + job_name);


while (index < documentNumber) {
    var date = new Date(minDate.getTime() + Math.random() * delta);
    var value = Math.random();
    var document = {
        created_at: date,
        valor: value
    };
    batchDocuments[index % batchNumber] = document;
    if ((index + 1) % batchNumber == 0) {
        db[documentName].insert(batchDocuments);
    }
    index++;
    if (index % 100000 == 0) {
        print(job_name + ' inserted ' + index + ' documents.');
    }
}

print(job_name + ' inserted ' + documentNumber + ' in ' + (new Date() - start) / 1000.0 + 's');


/**
 * Para ejecutar usar la consola
 * mongo random --eval "var name='random';arg1=50000000;arg2=1" create_random.js
 * @params
 *      arg1 cantidad de documentos a insertar
 *      arg2 nombre del job
 *      name nombre del doc en base
 *      batch cantidad a insertar al mismo tiempo (que se quedan en RAM)
 */